import { Emitter } from "../Common/Emitter"
import { gameOfLife, worms, Preset, stars, waves } from '../Config/Presets'

const presetsMap: Record<string, Preset> = {
  gameOfLife,
  worms,
  stars,
  waves,
}

export class Panel extends Emitter {
  matrixElement?: HTMLDivElement
  presetsSelector?: HTMLSelectElement
  matrix: number[] = []

  init (matrix: number[]) {
    this.matrix = matrix
    this.matrixElement = document.querySelector('[data-element="matrix"]') as HTMLDivElement
    this.presetsSelector = document.querySelector('[data-element="preset"]') as HTMLSelectElement
    this.presetsSelector.value = 'gameOfLife'

    this.initConvolutionMatrixFields(this.matrix)
    this.initHandlers()
  }

  initConvolutionMatrixFields (matrix: number[] = this.matrix) {
    if (!this.matrixElement) {
      return
    }

    let position = 0
    for (const value of matrix) {
      const element = this.matrixElement.querySelector(`[data-position="${position++}"]`) as HTMLInputElement
      element && (element.value = String(value))
    }
  }

  initHandlers () {
    if (!this.matrixElement) {
      return
    }

    this.matrixElement.addEventListener('change', this.handleChange.bind(this))
    this.presetsSelector!.addEventListener('change', this.handleChangePreset.bind(this))
  }

  handleChange (event: Event) {
    const target = event.target as HTMLInputElement
    const value = target.value
    const position = Number(target.dataset.position)

    this.matrix[position] = Number.isFinite(value)? Number(value) : 0

    this.emit('matrixChange', this.matrix)
  }

  handleChangePreset (event: Event) {
    const target = event.target as HTMLSelectElement
    this.emit('presetChange', presetsMap[target.value as string] || presetsMap.gameOfLife)
  }
}
