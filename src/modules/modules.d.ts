
type GlSlEditorConstructor = (...args: any[]) => GlSlEditor
type GlSlEditor = {}

declare module 'glsl-editor' {
  import GlSlEditor from 'glsl-editor'
  export default GlSlEditor as GlSlEditorConstructor
}
