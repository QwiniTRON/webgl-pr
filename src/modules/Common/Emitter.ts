
export type EmitterHandler = (...args: any[]) => any
export class Emitter {
  events: Record<string, EmitterHandler[]> = {}

  emit(event: string, ...args: any[]) {
    let callbacks = this.events[event] || []
    for (let i = 0, length = callbacks.length; i < length; i++) {
      callbacks[i](...args)
    }
  }

  on(event: any, cb: EmitterHandler) {
    this.events[event] && this.events[event].push(cb) || (this.events[event] = [cb])
    return () => {
      this.events[event] = this.events[event] && this.events[event].filter(i => cb !== i) || []
    }
  }
}
