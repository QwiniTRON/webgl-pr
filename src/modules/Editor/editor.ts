import { basicSetup, EditorView } from "codemirror";
import { cpp } from "@codemirror/lang-cpp";
import { debounce } from "lodash";
import { Emitter } from "../Common/Emitter";

const defaultEditorSelector = "#editor";

export class Editor extends Emitter {
  public editor!: EditorView;

  constructor(editorSelector: string = defaultEditorSelector) {
    super()
  }

  init (sourceText: string, editorSelector: string = defaultEditorSelector, ) {
    const config = {
      parent: document.querySelector(editorSelector),
      doc: sourceText || '',
      extensions: [
        basicSetup,
        cpp(),
        EditorView.updateListener.of(
          debounce(this.handleEditorChange.bind(this), 300)
        ),
      ],
      mode: "text/x-csrc",
      theme: "eclipse",
    };

    this.editor = new EditorView(config as any);
  }

  handleEditorChange (viewEvent: any) {
    if (viewEvent.docChanged) {
      const sourceText = viewEvent.state.doc.text.join('\n')

      this.emit('sourceTextChanged', sourceText)
    }
  }

  changeSoureText (text: string) {
    document.querySelector(defaultEditorSelector)!.innerHTML = ''
    this.init(text)
  }
}
