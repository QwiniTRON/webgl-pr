import { Preset } from "../Config";
import {
  ProgramData,
  activationProgramData,
  convolutionProgramData,
  copyProgramData,
  intrusionProgramData,
  texRendProgramData,
} from "./ProgramData";
import { Resolution } from "./Resolution";
import {
  ConvolutionProgram,
  IntrusionProgram,
  CopyProgram,
  TexRendProgram,
  ActivationProgram,
  Program,
} from "./Programs";
import { ShaderController } from "../ShaderController";
import { Color } from "../Config/Color";

enum MouseButton { left, mid, right }
export class IntrusionDescription {
  x: number;
  y: number;
  button: MouseButton;

  constructor(x: number, y: number, button: MouseButton) {
    this.x = x;
    this.y = y;
    this.button = button;
  }
}


export class Renderer {
  constructor(public gl: WebGL2RenderingContext) { }
  activationProgram!: ActivationProgram;
  convolutionProgram!: ConvolutionProgram;
  copyProgram!: CopyProgram;
  intrusionProgram!: IntrusionProgram;
  texRendProgram!: TexRendProgram;

  resolution!: Resolution

  backFB!: WebGLFramebuffer

  glDraw: any
  copy: any
  intrusionPhase: any
  convolutionPhase: any
  activationPhase: any
  drawToScreenPhase: any

  brushSize: number = 10

  createShader (type: GLenum, source: string): WebGLShader {
    var shader = this.gl.createShader(type);
    if (shader === null) {
      throw new Error("");
    }

    this.gl.shaderSource(shader, source);
    this.gl.compileShader(shader);
    var success = this.gl.getShaderParameter(shader, this.gl.COMPILE_STATUS);
    if (success) return shader;

    this.gl.deleteShader(shader);

    throw new Error(this.gl.getShaderInfoLog(shader) || "");
  }

  createProgram (vert: WebGLShader, frag: WebGLShader): WebGLProgram {
    var program = this.gl.createProgram();
    if (program === null) {
      throw new Error("no program");
    }

    this.gl.attachShader(program, vert);
    this.gl.attachShader(program, frag);
    this.gl.linkProgram(program);
    var success = this.gl.getProgramParameter(program, this.gl.LINK_STATUS);
    if (success) return program;

    this.gl.deleteProgram(program);

    throw new Error(this.gl.getProgramInfoLog(program) || "some cause");
  }

  init (preset: Preset, resolution: Resolution = Resolution.default) {
    const self = this

    this.resolution = resolution

    const backgroundColor = Color.black;
    const brushColor = Color.aqua;

    const activationFrag = ShaderController.getActivationFrag(
      preset.activationFunction
    ) as string;

    const act_v = this.createShader(
      this.gl.VERTEX_SHADER,
      activationProgramData.vert
    );
    const act_f = this.createShader(
      this.gl.FRAGMENT_SHADER,
      activationFrag
    );

    this.activationProgram = new ActivationProgram(
      this,
      this.createProgram(act_v, act_f),
      activationProgramData
    );

    const int_v = this.createShader(
      this.gl.VERTEX_SHADER,
      intrusionProgramData.vert as string
    );
    const int_f = this.createShader(
      this.gl.FRAGMENT_SHADER,
      intrusionProgramData.frag as string
    );
    this.intrusionProgram = new IntrusionProgram(
      this,
      this.createProgram(int_v, int_f),
      intrusionProgramData
    );

    const conv_v = this.createShader(
      this.gl.VERTEX_SHADER,
      convolutionProgramData.vert as string
    );
    const conv_f = this.createShader(
      this.gl.FRAGMENT_SHADER,
      convolutionProgramData.frag as string
    );
    this.convolutionProgram = new ConvolutionProgram(
      this,
      this.createProgram(conv_v, conv_f),
      convolutionProgramData
    );

    // copy
    const copyv = this.createShader(
      this.gl.VERTEX_SHADER,
      copyProgramData.vert as string
    );
    const copyf = this.createShader(
      this.gl.FRAGMENT_SHADER,
      copyProgramData.frag as string
    );
    this.copyProgram = new CopyProgram(
      this,
      this.createProgram(copyv, copyf),
      copyProgramData
    );

    //tex rend
    this.texRendProgram = new TexRendProgram(
      this,
      this.createProgram(
        this.createShader(
          this.gl.VERTEX_SHADER,
          texRendProgramData.vert as string
        ),
        this.createShader(
          this.gl.FRAGMENT_SHADER,
          texRendProgramData.frag as string
        )
      ),
      texRendProgramData
    );

    const intrusionProgram = this.intrusionProgram.program;
    const convolutionProgram = this.convolutionProgram.program;
    const activationProgram = this.activationProgram.program;
    const copyProgram = this.copyProgram.program;
    const textureProgram = this.texRendProgram.program;

    let drawResolutionX = resolution.x;
    let drawResolutionY = resolution.y;

    this.gl.getExtension("EXT_color_buffer_float");

    // bind element buffer globally
    const el_buffer = this.gl.createBuffer();
    // create a shared VAO
    const vao = this.gl.createVertexArray();
    // create a shared vertex buffer
    const vertexBuffer = this.gl.createBuffer();

    // global initialization
    const elementIndices = new Uint8Array([0, 2, 1, 1, 2, 3]);
    const vertex_rect = new Float32Array([
      -1.0,
      1.0, // top left
      1.0,
      1.0, // top right
      -1.0,
      -1.0, // bottom left
      1.0,
      -1.0, // bottom right
    ]);
    this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, el_buffer);
    this.gl.bufferData(
      this.gl.ELEMENT_ARRAY_BUFFER,
      elementIndices,
      this.gl.STATIC_DRAW
    );
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, vertexBuffer);
    this.gl.bufferData(
      this.gl.ARRAY_BUFFER,
      new Float32Array(vertex_rect),
      this.gl.STATIC_DRAW
    );

    // create framebuffer to render to texture
    const backFB = this.gl.createFramebuffer();
    this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, backFB);
    this.backFB = backFB!

    // convolution
    const convTexture = this.gl.createTexture();
    this.gl.bindTexture(this.gl.TEXTURE_2D, convTexture);
    this.gl.texImage2D(
      this.gl.TEXTURE_2D,
      0,
      this.gl.R32F,
      drawResolutionX,
      drawResolutionY,
      0,
      this.gl.RED,
      this.gl.FLOAT,
      new Float32Array(
        Array.from(
          { length: drawResolutionX * drawResolutionY },
          (_, key) => 0.0
        )
      )
    );
    this.gl.texParameteri(
      this.gl.TEXTURE_2D,
      this.gl.TEXTURE_MIN_FILTER,
      this.gl.NEAREST
    );
    this.gl.texParameteri(
      this.gl.TEXTURE_2D,
      this.gl.TEXTURE_MAG_FILTER,
      this.gl.NEAREST
    );

    // activation
    const actTexture = this.gl.createTexture();
    this.gl.bindTexture(this.gl.TEXTURE_2D, actTexture);
    this.gl.texImage2D(
      this.gl.TEXTURE_2D,
      0,
      this.gl.R32F,
      drawResolutionX,
      drawResolutionY,
      0,
      this.gl.RED,
      this.gl.FLOAT,
      new Float32Array(
        Array.from({ length: drawResolutionX * drawResolutionY }, (_, key) =>
          Math.random()
        )
      )
    );

    this.gl.texParameteri(
      this.gl.TEXTURE_2D,
      this.gl.TEXTURE_MIN_FILTER,
      this.gl.NEAREST
    );
    this.gl.texParameteri(
      this.gl.TEXTURE_2D,
      this.gl.TEXTURE_MAG_FILTER,
      this.gl.NEAREST
    );

    const convolutionMatrix = preset.convolution;

    // intrusion program uniforms
    const u_intrusion = this.gl.getUniformLocation(
      intrusionProgram,
      "u_intrusion"
    );
    const u_intrusion_radius = this.gl.getUniformLocation(
      intrusionProgram,
      "u_intrusion_radius"
    );
    const u_ones = this.gl.getUniformLocation(intrusionProgram, "u_ones");
    const field = this.gl.getUniformLocation(intrusionProgram, "field");

    // activation program uniforms
    const u_act_source = this.gl.getUniformLocation(
      activationProgram,
      "act_source"
    );

    // convilution program uniforms
    const u_convolution_matrix = this.gl.getUniformLocation(
      convolutionProgram,
      "u_convolution_matrix"
    );
    const u_conv_source = this.gl.getUniformLocation(
      convolutionProgram,
      "conv_source"
    );

    // texture program uniforms
    const u_zero_color = this.gl.getUniformLocation(
      textureProgram,
      "u_zero_color"
    );
    const u_one_color = this.gl.getUniformLocation(
      textureProgram,
      "u_one_color"
    );
    const activated_values = this.gl.getUniformLocation(
      textureProgram,
      "activated_values"
    );

    function glDraw (gl: WebGL2RenderingContext) {
      gl.clearColor(0, 0, 0, 0);
      gl.clear(gl.COLOR_BUFFER_BIT);
      gl.bindVertexArray(vao);
      gl.vertexAttribPointer(0, 2, gl.FLOAT, false, 2 * 4, 0);
      gl.enableVertexAttribArray(0);
      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, el_buffer);
      gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
      gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_BYTE, 0);
    }
    this.glDraw = glDraw

    function copy (
      gl: WebGL2RenderingContext,
      from: WebGLTexture,
      to: WebGLTexture
    ) {
      gl.framebufferTexture2D(
        gl.FRAMEBUFFER,
        gl.COLOR_ATTACHMENT0,
        gl.TEXTURE_2D,
        to,
        0
      );
      gl.useProgram(copyProgram);
      gl.uniform1i(u_orig, 1);
      gl.activeTexture(gl.TEXTURE0 + 1);
      gl.bindTexture(gl.TEXTURE_2D, from);
      glDraw(gl);
    }
    this.copy = copy

    function intrusionPhase(
      gl: WebGL2RenderingContext,
      intrusion: IntrusionDescription
    ) {
      // smartly use convolution texture as an intermediate storage texture
      gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, convTexture, 0);
      gl.useProgram(intrusionProgram);
      gl.uniform2i(u_intrusion, intrusion.x, intrusion.y);
      gl.uniform1i(u_intrusion_radius, self.brushSize);
      gl.uniform1i(u_ones, intrusion.button == MouseButton.left ? 1 : 0);
      gl.uniform1i(field, 1);
      gl.activeTexture(gl.TEXTURE0 + 1);
      gl.bindTexture(gl.TEXTURE_2D, actTexture);
      glDraw(gl);

      // copy result to activation texture once we are done intruding
      copy(gl, convTexture!, actTexture!);
    }

    this.intrusionPhase = intrusionPhase

    function convolutionPhase (gl: WebGL2RenderingContext) {
      gl.framebufferTexture2D(
        gl.FRAMEBUFFER,
        gl.COLOR_ATTACHMENT0,
        gl.TEXTURE_2D,
        convTexture,
        0
      );
      gl.useProgram(convolutionProgram);
      gl.uniform1fv(u_convolution_matrix, convolutionMatrix);
      gl.uniform1i(u_conv_source, 1);
      gl.activeTexture(gl.TEXTURE0 + 1);
      gl.bindTexture(gl.TEXTURE_2D, actTexture);
      glDraw(gl);
    }
    this.convolutionPhase = convolutionPhase

    function activationPhase (gl: WebGL2RenderingContext) {
      gl.framebufferTexture2D(
        gl.FRAMEBUFFER,
        gl.COLOR_ATTACHMENT0,
        gl.TEXTURE_2D,
        actTexture,
        0
      );
      gl.useProgram(activationProgram);
      gl.uniform1i(u_act_source, 1);
      gl.activeTexture(gl.TEXTURE0 + 1);
      gl.bindTexture(gl.TEXTURE_2D, convTexture);
      glDraw(gl);
    }
    this.activationPhase = activationPhase

    function drawToScreenPhase (gl: WebGL2RenderingContext) {
      gl.useProgram(textureProgram);
      gl.uniform4f(
        u_zero_color,
        backgroundColor.r,
        backgroundColor.g,
        backgroundColor.b,
        backgroundColor.a
      );
      gl.uniform4f(
        u_one_color,
        brushColor.r,
        brushColor.g,
        brushColor.b,
        brushColor.a
      );
      gl.uniform1i(activated_values, 1);
      gl.activeTexture(gl.TEXTURE0 + 1);
      gl.bindTexture(gl.TEXTURE_2D, actTexture);
      glDraw(gl);
    }
    this.drawToScreenPhase = drawToScreenPhase

    // copy program uniforms
    const u_orig = this.gl.getUniformLocation(copyProgram, "orig");
  }

  cycle(
    actConvRun: boolean,
    intrusion?: IntrusionDescription,
  ) {
    // first make sure we are rendering to texture
    this.gl.viewport(0, 0, this.resolution.x, this.resolution.y);
    this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.backFB);

    // intrude if there is something to intrude with
    if (intrusion != null) {
      this.intrusionPhase(this.gl, intrusion);
    }

    // actual percieve-react cycle
    if (actConvRun) {
      this.convolutionPhase(this.gl);
      this.activationPhase(this.gl);
    }

    // then make sure we are rendering to screen
    this.gl.viewport(0, 0, this.resolution.realX, this.resolution.realY);
    this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null);
    this.drawToScreenPhase(this.gl);
  }


  getField (program: Program, field: string): WebGLUniformLocation | null {
    return this.gl.getUniformLocation(program.program, field);
  }
}
