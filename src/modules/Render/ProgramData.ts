import convv from '~/shaders/convolution.vert'
import convf from '~/shaders/convolution.frag'
import intv from '~/shaders/intrusion.vert'
import intf from '~/shaders/intrusion.frag'
import texv from '~/shaders/tex_rend.vert'
import texf from '~/shaders/tex_rend.frag'
import copyf from '~/shaders/copy.frag'
import copyv from '~/shaders/copy.vert'
import actv from '~/shaders/activation.vert'
import actf from '~/shaders/activation.frag'

export class ProgramData {
  constructor (public vert: string, public frag: string) {}
}

// #region default programms
export const convolutionProgramData = new ProgramData(convv, convf)
export const intrusionProgramData = new ProgramData(intv, intf)
export const texRendProgramData = new ProgramData(texv, texf)
export const copyProgramData = new ProgramData(copyv, copyf)
export const activationProgramData = new ProgramData(actv, actf)
// #endregion default programs

