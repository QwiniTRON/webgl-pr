
export class Resolution {
  constructor (public x: number, public y: number, public realX: number, public realY: number) {}

  static get default () {
    return new this(512, 512, 512, 512)
  }
}
