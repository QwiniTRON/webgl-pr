import type { ProgramData } from "../ProgramData";
import type { Renderer } from "../Renderer";
import { Program } from "./Program";

export class IntrusionProgram extends Program {
  intrusionRadius!: WebGLUniformLocation | null
  intrusion!: WebGLUniformLocation | null

  constructor (public renderer: Renderer, public program: WebGLProgram, public data: ProgramData) {
    super(renderer, program, data)

    this.intrusionRadius = renderer.getField(this, 'u_intrusion_radius')
    this.intrusion = renderer.getField(this, 'u_intrusion')
  }
}
