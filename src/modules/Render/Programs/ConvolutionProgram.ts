import { ProgramData } from "../ProgramData";
import type { Renderer } from "../Renderer";
import { Program } from "./Program";

export class ConvolutionProgram extends Program {
  convSource!: WebGLUniformLocation | null
  convolutionMatrix!: WebGLUniformLocation | null

  constructor (public renderer: Renderer, public program: WebGLProgram, public data: ProgramData) {
    super(renderer, program, data)

    this.convSource = renderer.getField(this, 'conv_source')
    this.convolutionMatrix = renderer.getField(this, 'u_convolution_matrix')
  }
}
