import type { ProgramData } from "../ProgramData";
import type { Renderer } from "../Renderer";
import { Program } from "./Program";

export class TexRendProgram extends Program {
  zeroColor!: WebGLUniformLocation | null
  oneColor!: WebGLUniformLocation | null

  constructor (public renderer: Renderer, public program: WebGLProgram, public data: ProgramData) {
    super(renderer, program, data)

    this.zeroColor = renderer.getField(this, 'u_zero_color')
    this.oneColor = renderer.getField(this, 'u_one_color')
  }
}
