import type { ProgramData } from "../ProgramData";
import type { Renderer } from "../Renderer";
import { Program } from "./Program";

export class CopyProgram extends Program {
  orig!: WebGLUniformLocation | null

  constructor (public renderer: Renderer, public program: WebGLProgram, public data: ProgramData) {
    super(renderer, program, data)

    this.orig = renderer.getField(this, 'orig')
  }
}
