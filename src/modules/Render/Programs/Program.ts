import { ProgramData } from "../ProgramData";
import type { Renderer } from "../Renderer";

export abstract class Program {
  constructor (public renderer: Renderer, public program: WebGLProgram, public data: ProgramData) {}
}


