import { ProgramData } from "../ProgramData";
import { Program } from "./Program";
import type { Renderer } from "../Renderer";

export class ActivationProgram extends Program {
  actSource!: WebGLUniformLocation | null

  constructor (public renderer: Renderer, public program: WebGLProgram, public data: ProgramData) {
    super(renderer, program, data)

    this.actSource = renderer.getField(this, 'act_source')
  }
}
