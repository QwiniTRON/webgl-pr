#version 300 es

precision mediump float;

in vec2 f_position;
out float out_value;

uniform sampler2D act_source;

<%= activation %>

void main() {
  out_value = activation(float(texture(act_source, f_position)));
}

