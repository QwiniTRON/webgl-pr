import activationTemplate from './activation.tpl'
import _ from 'lodash'

const activationTemplateCompiled = _.template(activationTemplate)

export class ShaderController {
  static getActivationFrag (activationFnString: string) {
    if (!activationFnString) {
      throw new Error('activationFnString was undefined')
    }

    return activationTemplateCompiled({activation: activationFnString})
  }
}
