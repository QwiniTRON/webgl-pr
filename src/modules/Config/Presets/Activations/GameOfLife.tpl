float activation(float _input) {
  if (_input == -7. || _input == -6. || _input == 3.) {
    return 1.0;
  }

  return 0.0;
}