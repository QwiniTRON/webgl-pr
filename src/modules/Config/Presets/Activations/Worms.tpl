float inverse_gaussian(float x) {
  return -1./pow(2., (0.6*pow(x, 2.)))+1.;
}

float activation(float x) {
  return inverse_gaussian(x);
}