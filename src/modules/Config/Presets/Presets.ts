import gameOfLifeActivationFunction from './Activations/GameOfLife.tpl'
import wormsActivationFunction from './Activations/Worms.tpl'
import wavesActivationFunction from './Activations/Waves.tpl'
import starsActivationFunction from './Activations/Starts.tpl'

interface IClonable<T> {
  clone(): T
}

export class Preset implements IClonable<Preset> {
  public convolution: Float32Array;
  public activationFunction: string;

  constructor(convolution: Float32Array, activationFunction: string) {
    this.convolution = convolution;
    this.activationFunction = activationFunction;
  }

  clone () {
    return new Preset(this.convolution, this.activationFunction)
  }
}

export const gameOfLife = new Preset(
  new Float32Array([1, 1, 1, 1, -9, 1, 1, 1, 1]),
  gameOfLifeActivationFunction
);

export const worms = new Preset(
  new Float32Array([0.68, -0.9, 0.68, -0.9, -0.66, -0.9, 0.68, -0.9, 0.68]),
  wormsActivationFunction
);

export const waves = new Preset(
  new Float32Array([0.6, -0.7, 0.6, -0.7, 0.63, -0.7, 0.6, -0.7, 0.6]),
  wavesActivationFunction
);

export const stars = new Preset(
  new Float32Array([0.565, -0.716, 0.565, -0.759, 0.627, -0.759, 0.565, -0.716, 0.565]),
  starsActivationFunction
);
