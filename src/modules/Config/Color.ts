export type ColorConstructorArguments = {
  r: number;
  g: number;
  b: number;
  a: number;
};

export class Color {
  public r: number;
  public g: number;
  public b: number;
  public a: number;

  constructor(
    {
      r = 0,
      g = 0,
      b = 0,
      a = 1,
    }: ColorConstructorArguments = {} as ColorConstructorArguments
  ) {
    this.r = r;
    this.g = g;
    this.b = b;
    this.a = a;
  }

  static get black() {
    return new this();
  }

  static get aqua() {
    return new this({ r: 0, g: 1.0, b: 1.0, a: 1.0 });
  }

  static get red() {
    return new this({ r: 1, g: 0, b: 0, a: 0 });
  }

  getComponents () {
    return [this.r, this.g, this.b, this.a]
  }
}
