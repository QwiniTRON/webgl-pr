import "./styles/index.scss";
import "./modules";
import {
  Renderer,
  worms as wormsPreset,
  gameOfLife,
  ShaderController,
  Resolution,
  Preset,
} from "./modules";
import _, { debounce } from "lodash";
import { Editor } from "./modules/Editor";
import { IntrusionDescription } from "./modules/Render/Renderer";
import { Panel } from "./modules/Panel";

enum MouseButton {
  left,
  mid,
  right,
}

// NOTICE THAT ACTIVATION FIUNCTION SHOUE RETURNB FROM .0.0 tpo 1.0!!;

// position vertices are location=0, tex_coord_positions are location=1 throughout the app!!!
function main() {
  const canvas = document.getElementById("view") as HTMLCanvasElement;
  canvas.oncontextmenu = () => false;
  const gl = canvas.getContext("webgl2");
  const renderer = new Renderer(gl!);
  const editor = new Editor();
  const panel = new Panel();
  let preset = gameOfLife.clone();

  if (!gl) return;
  let rect = canvas.getBoundingClientRect();
  let resolution = makeResolution()
  let isPlaying = false;

  window.addEventListener("resize", debounce(() => {
    rect = canvas.getBoundingClientRect();
    resolution = makeResolution()
    restart()
  }, 300));

  gl.getExtension("EXT_color_buffer_float");

  // #region handlers
  let events: MouseEvent[] = [];

  editor.on(
    "sourceTextChanged",
    debounce(function (sourceText) {
      try {
        preset.activationFunction = sourceText;
        restart();
      } catch (error) {
        // console.error(error);
      }
    }, 300)
  );
  panel.on(
    "matrixChange",
    debounce(function (matrix: number[]) {
      try {
        preset.convolution = new Float32Array(matrix);
        restart();
      } catch (error) {
        // console.error(error);
      }
    }, 300)
  );
  panel.on("presetChange", (newPreset: Preset) => {
    if (!newPreset) {
      return;
    }

    try {
      Object.assign(preset, newPreset);
      panel.matrix = Array.from(preset.convolution)
      panel.initConvolutionMatrixFields()
      editor.changeSoureText(newPreset.activationFunction)
      restart();
    } catch (error) {
      // console.error(error);
    }
  });

  const stepButton = document.getElementById("step") as HTMLDivElement;
  stepButton?.addEventListener("click", (e) => {
    frame(gl);
  });

  const doButton = document.getElementById("do") as HTMLDivElement;
  doButton?.addEventListener("click", (e) => {
    doButton.classList.toggle("_ended");
    isPlaying = !isPlaying;
    stepButton.style.display = isPlaying ? "none" : "inline";
    frame(gl);
  });

  canvas.addEventListener("mousedown", (e) => {
    events.push(e);
    frame(gl);
  });

  canvas.addEventListener("mouseup", (e) => {
    // events.push(e)
  });

  function restart() {
    renderer.init(preset, resolution);
    frame(gl!);
  }

  const restartButton = document.querySelector(
    '[data-element="restart"]'
  ) as HTMLDivElement;
  restartButton &&
    restartButton.addEventListener("click", (event) => {
      restart();
    });

  // #endregion handlers

  function makeResolution () {
    return new Resolution(
      rect.width,
      rect.height,
      rect.width,
      rect.height
    );
  }

  renderer.init(preset, resolution);
  panel.init(Array.from(preset.convolution));
  editor.init(preset.activationFunction as string);

  function getRectRelative(targetRect: any, windowRect: any) {
    return {
      x: windowRect.x - targetRect.x,
      y: windowRect.y - targetRect.y,
    };
  }

  function frame(gl: WebGL2RenderingContext) {
    const lastEvent = events.pop();

    if (lastEvent) {
      // console.log(lastEvent);
      const { x, y } = getRectRelative(rect, lastEvent);
      const drawX = Math.floor(resolution.x * (x / rect.width));
      const drawY = Math.floor(resolution.y - resolution.y * (y / rect.height));
      renderer.cycle(
        false,
        new IntrusionDescription(
          drawX,
          drawY,
          lastEvent.button == 0 ? MouseButton.left : MouseButton.right
        )
      );
    } else {
      renderer.cycle(true);
    }

    if (!isPlaying) {
      return;
    }

    requestAnimationFrame(() => {
      frame(gl);
    });
  }
  frame(gl);
}

window.addEventListener("load", main);
