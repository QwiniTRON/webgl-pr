#version 300 es

precision mediump float;

in vec2 f_position;
out float out_value;

uniform float u_convolution_matrix[9];
uniform sampler2D conv_source;

void main() {
  ivec2 ts = textureSize(conv_source, 0);
  vec2 texel = vec2(
    1.0 / float(ts.x),
    1.0 / float(ts.y)
  );

  vec2 tl = vec2(
      mod(f_position.x - texel.x, 1.0),
      mod(f_position.y + texel.y, 1.0)
  );
  vec2 tc = vec2(
      f_position.x,
      mod(f_position.y + texel.y, 1.0)
  );
  vec2 tr = vec2(
      mod(f_position.x + texel.x, 1.0),
      mod(f_position.y + texel.y, 1.0)
  );
  vec2 cl = vec2(
      mod(f_position.x - texel.x, 1.0),
      f_position.y
  );
  vec2 cc = vec2(
    mod(f_position.x, 1.0),
    f_position.y
  );
  vec2 cr = vec2(
    mod(f_position.x + texel.x, 1.0),
    f_position.y
  );
  vec2 bl = vec2(
    mod(f_position.x - texel.x, 1.0),
    mod(f_position.y - texel.y, 1.0)
  );
  vec2 bc = vec2(
    mod(f_position.x, 1.0),
    mod(f_position.y - texel.y, 1.0)
  );
  vec2 br = vec2(
    mod(f_position.x + texel.x, 1.0),
    mod(f_position.y - texel.y, 1.0)
  );

  float conv_res = 
      u_convolution_matrix[0] * float(texture(conv_source, tl)) +
      u_convolution_matrix[1] * float(texture(conv_source, tc)) +
      u_convolution_matrix[2] * float(texture(conv_source, tr)) +
      u_convolution_matrix[3] * float(texture(conv_source, cl)) +
      u_convolution_matrix[4] * float(texture(conv_source, cc)) +
      u_convolution_matrix[5] * float(texture(conv_source, cr)) +
      u_convolution_matrix[6] * float(texture(conv_source, bl)) +
      u_convolution_matrix[7] * float(texture(conv_source, bc)) +
      u_convolution_matrix[8] * float(texture(conv_source, br));

  out_value = conv_res;
}
