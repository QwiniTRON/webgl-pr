#version 300 es

precision mediump float;

in vec2 f_pos;
out float out_value;

uniform sampler2D orig;

void main() {
  out_value = float(texture(orig, f_pos));
}