#version 300 es

precision mediump float;

in vec2 f_TexCoords;
out vec4 out_color;

uniform vec4 u_zero_color;
uniform vec4 u_one_color;

uniform sampler2D activated_values;

vec4 lerpv4(vec4 a, vec4 b, float t) {
  return a + (b - a) * t;
}

void main() {
  float sampled = float(texture(activated_values, f_TexCoords));
  out_color = lerpv4(u_zero_color, u_one_color, sampled);
}