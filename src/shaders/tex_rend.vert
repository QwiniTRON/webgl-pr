#version 300 es

layout(location=0) in vec4 a_position;

out vec2 f_TexCoords;

void main() {
  gl_Position = a_position;
  f_TexCoords = (a_position.xy / 2.) + .5;
}