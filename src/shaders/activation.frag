#version 300 es

precision mediump float;

in vec2 f_position;
out float out_value;

uniform sampler2D act_source;

float activation(float _input) {
  // Any live cell with two or three live neighbours survives.
  // Any dead cell with three live neighbours becomes a live cell.
  // All other live cells die in the next generation. Similarly, all other dead cells stay dead.

  // _input < 0.0    --->    live cell
  // _input >= 0.0    --->    dead cell

  if (_input == -7. || _input == -6. || _input == 3.) {
    return 1.0;
  }

  return 0.0;
}

// an inverted gaussian function,
// where f(0) = 0.
// Graph: https://www.desmos.com/calculator/torawryxnq
// float inverse_gaussian(float x) {
//   return -1./pow(2., (0.6*pow(x, 2.)))+1.;
// }

// float activation(float x) {
//   return inverse_gaussian(x);
// }

void main() {
  out_value = activation(float(texture(act_source, f_position)));
}
