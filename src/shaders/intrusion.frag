#version 300 es

precision mediump float;

in vec2 f_position;
out float out_value;

uniform int u_intrusion_radius;
uniform ivec2 u_intrusion;
uniform bool u_ones;

uniform sampler2D field;

void main() {
  ivec2 ts = textureSize(field, 0);
  ivec2 res = ivec2(
    int(f_position.x * float(ts.x)),
    int(f_position.y * float(ts.y))
  );

  ivec2 ui = u_intrusion;
  int dx = abs(ui.x - res.x);
  int dy = abs(ui.y - res.y);

  int r = u_intrusion_radius;
  if (ui.x >= 0 && ui.y >= 0 && (dx <= r || dx >= ts.x - r) && (dy <= r || dy >= ts.y - r)) {
    out_value = u_ones ? 1.0 : 0.0;
  } else {
    out_value = float(texture(field, f_position));
  }
}
