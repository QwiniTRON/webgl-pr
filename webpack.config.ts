import * as path from "path"
import HTMLWebpackPlugin from "html-webpack-plugin"
import * as webpack from "webpack"

const plugins: any[] = []

plugins.push(
  new HTMLWebpackPlugin({
    template: path.resolve(__dirname, "public", "index.html"),
  })
);

const config: webpack.Configuration = {
  entry: { index: path.resolve(__dirname, "src", "index.ts") },
  output: { path: path.resolve(__dirname, "build"), filename: "main.js" },
  plugins: plugins,

  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
    alias: {
      '~': path.resolve(__dirname, 'src'),
    },
  },

  module: {
    rules: [
      {
        exclude: /(node_modules)/,
        test: /\.(vert|frag|tpl)$/i,
        use: 'raw-loader',
      },
      {
        exclude: /(node_modules)/,
        test: /\.[tj]sx?$/,
        use: ['babel-loader', "ts-loader"],
      },
      {
        exclude: /(node_modules)/,
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
      {
        exclude: /(node_modules)/,
        test: /\.s[ac]ss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
    ],
  },
};

export default config;
